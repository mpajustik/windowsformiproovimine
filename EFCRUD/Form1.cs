﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EFCRUD
{
    public partial class Form1 : Form
    {
        ContactEntities db;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            db = new ContactEntities();
            contactBindingSource.DataSource = db.Contacts.ToList();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            using (frmAddEditContact frm = new frmAddEditContact(null))
            {
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    contactBindingSource.DataSource = db.Contacts.ToList();
                }
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (contactBindingSource.Current == null)
            {
                return;
            }
            using (frmAddEditContact frm = new frmAddEditContact(contactBindingSource.Current as Contact))
            {
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    contactBindingSource.DataSource = db.Contacts.ToList();
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (contactBindingSource.Current != null)
            {
                if (MessageBox.Show("Are you sure you want to delete this record", "Message", MessageBoxButtons.YesNo,MessageBoxIcon.Question)==DialogResult.Yes)
                {
                    db.Contacts.Remove(contactBindingSource.Current as Contact);
                    contactBindingSource.RemoveCurrent();
                    db.SaveChanges();
                }
            }
        }
    }
}
